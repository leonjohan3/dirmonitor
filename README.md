[TOC]

# Overview
This utility/[component](https://en.wikipedia.org/wiki/Component-based_software_engineering#Software_component) monitors the [file system](https://en.wikipedia.org/wiki/File_system) for changes. If there had been any changes to the filesystem since the last check, it sends an [event message](https://en.wikipedia.org/wiki/Event_(computing)).
Periodically the component will read a root folder and all sub folders with the lists of files and compare that to a previous [snapshot](https://en.wikipedia.org/wiki/Snapshot_(computer_storage)), and after comparing the two,
send an event message with a list of differences. This git repo shows a minimalist implementation. This utility is used in conjunction with [tojmsq](https://bitbucket.org/leonjohan3/tojmsq) and [fromjmsq](https://bitbucket.org/leonjohan3/fromjmsq).

This component might be useful for [system integration](https://en.wikipedia.org/wiki/System_integration), but there are probably better ways to do systems integration.

# Procedure
1.  Read the previous snapshot from file into memory. The collection of files will be empty (the snapshot file will not exist) on the first iteration (see the safe mode option below). The historical snapshot file contains a [serialized ](https://en.wikipedia.org/wiki/Serialization) [hashmap](https://docs.oracle.com/javase/7/docs/api/java/util/HashMap.html) of [file properties](https://bitbucket.org/leonjohan3/dirmonitor/src/master/#markdown-header-file-properties).
2.  From the root folder, read all files and sub folders, [recursing](https://en.wikipedia.org/wiki/Recursion_(computer_science)) up to the maximum configured depth, constructing a new map of the current state of the filesystem.
3.  Compare each entry with it's corresponding entry in the previous snapshot read in 1 above.
4.  Create a 3rd map in memory with the differences found.
5.  If any differences are found, construct (and validate if using XML) and send event message(s) to the JMS destination and persist the new map.

# Configuration parameters
* Root folder (default being the current working folder for the process).
* Lock filename, default .lockfile in the root folder. While the component has this file locked, other systems should pause and not update the filesystem under the root folder.
* List of file file name [regular expressions](https://en.wikipedia.org/wiki/Regular_expression) (defaulting to null, considering all files).
* Maximum folder level depth, with a default of 256 (unimplemented).
* Snapshot storage location. These snapshot files should be located outside the root folder structure and only the owner should have write access to the hosting folder. This implies that only the owner of the snapshot files will be able to run this program. The user running the program should only have read rights in the root folder structure.
* Maximum number of historical snapshots to keep with a default of 3. Historical snapshots could be useful for troubleshooting.
* [JMS](https://en.wikipedia.org/wiki/Java_Message_Service) connection details and JMS destination (being either a [queue](https://en.wikipedia.org/wiki/Message_queue) or a [topic](https://en.wikipedia.org/wiki/Java_Message_Service#Publish/subscribe_model)). Use [jasypt](http://www.jasypt.org) to encrypt the credentials using "Xoh3ieso" as the key.
* Event message format: byte array message with Java hash map (the default) or XML string message (see XML Schema and [example](https://bitbucket.org/leonjohan3/dirmonitor/src/master/#markdown-header-event-message-contents) below).
* Safe mode change percentage, with a default of 60%. Ensure that the total number of changes (new files, deleted files and files modified) do not exceed this percentage.

# File properties
1.  Absolute file name.
2.  Time last updated.
3.  File size.
4.  [CRC-32](https://en.wikipedia.org/wiki/Cyclic_redundancy_check) hash of the file contents.

# Event message contents
A [map](https://en.wikipedia.org/wiki/Associative_array) with:

* Key: string, absolute file name.
* Value: [enum](https://en.wikipedia.org/wiki/Enumerated_type), change, new (new file that did not exist in the previous snapshot), modified (file was present in the previous snapshot, but the file's content/hash value had changed), deleted (file was present in the previous snapshot, but not in the current).

# Constraints
* The complete filesystem (folder hierarchy) needs to fit in memory. In fact, both the original snapshot and historical snapshot of the filesystems need to fit into memory simultaneously for comparison.
* Changes to the filesystem should not be made while the component compares the current snapshot of the filesystem with a historical one, as this could cause [race conditions](https://en.wikipedia.org/wiki/Race_condition). The lock file facility should always be used. If timing of updates cannot be controlled, an alternative integration method should be used.
* Downstream systems that consume the event messages off the JMS destination, must be able to handle duplicate file change event messages arriving in an unspecified order.

# Event message XML Schema
```xml
<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema" targetNamespace="http://www.example.org/fileSystemChangedEvent" xmlns:tns="http://www.example.org/fileSystemChangedEvent" elementFormDefault="qualified">

    <element name="FileSystemChangedEvent">
        <complexType>
            <sequence>
                <element name="File" maxOccurs="unbounded">
                    <complexType>
                        <sequence>
                            <element name="Name">
                                <simpleType>
                                    <restriction base="token">
                                        <minLength value="1" />
                                    </restriction>
                                </simpleType>
                            </element>
                            <element name="Change">
                                <simpleType>
                                    <restriction base="token">
                                        <enumeration value="New" />
                                        <enumeration value="Modified" />
                                        <enumeration value="Deleted" />
                                    </restriction>
                                </simpleType>
                            </element>
                        </sequence>
                    </complexType>
                </element>
            </sequence>
            <attribute name="schemaVersion" use="required">
                <simpleType>
                    <restriction base="float">
                        <minInclusive value="1.0" />
                        <maxExclusive value="2.0" />
                    </restriction>
                </simpleType>
            </attribute>
        </complexType>
        <unique name="EnsureUniqueFileName">
            <selector xpath="tns:File" />
            <field xpath="tns:Name" />
        </unique>
    </element>
</schema>
```

Example:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<FileSystemChangedEvent xmlns="http://www.example.org/fileSystemChangedEvent" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.example.org/fileSystemChangedEvent FileSystemChangedEvent-1p0.xsd" schemaVersion="1.0">
    <File>
        <Name>/full/path/name/to/file_1.txt</Name>
        <Change>New</Change>
    </File>
    <File>
        <Name>/full/path/name/to/file_2.txt</Name>
        <Change>Modified</Change>
    </File>
    <File>
        <Name>/full/path/name/to/file_3.txt</Name>
        <Change>Deleted</Change>
    </File>
</FileSystemChangedEvent>
```

# [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) [daemon](https://en.wikipedia.org/wiki/Daemon_(computing)) script
* TODO.

# [Systemd](https://en.wikipedia.org/wiki/Systemd) configuration
* TODO.

# Projects in Git
* https://bitbucket.org/leonjohan3/tojmsq
* https://bitbucket.org/leonjohan3/fromjmsq

# Good to know
* A seperate artifact for the XML Schema is available when this project is built with [maven](https://en.wikipedia.org/wiki/Apache_Maven) that downstream components can use for validating event messages in XML format.
* Technologies used: Spring boot and batch, XProc, Apache Commons IO, JAXB, java.nio.channels.FileLock, Checkstyle, Commons CLI, Duplicate code detection, FindBugs, JDepend, Rat Report.
* Hidden and system files will not be considered.
* Symbolic links will not be followed and/or considered.
* The access mode of the snapshot file will be change to read-only when persisting.
* For troubleshooting a utility should be available to dump a snapshot file to text, so that historical snapshots can be compared using a diff tool.
* https://docs.spring.io/spring/docs/current/spring-framework-reference/integration.html#jms
