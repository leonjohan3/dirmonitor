package org.example.directory;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Comparator {
    private static final int ARG_COUNT = 2;

    private String serializeFile;
    private String rootFolder;

    private Comparator() {
    }

    public static void main(final String[] args) {
        new Comparator().run(args);
    }

    private void run(final String[] args) {

        try {

            if (args.length < ARG_COUNT) {
                usage();
            } else {

                parseArgs(args);
                final Map<String, Entry> entries = new HashMap<>();

                // get previous state
                if (new File(serializeFile).exists()) {

                    try (FileInputStream fileInputStream = new FileInputStream(serializeFile)) {

                        try (ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {

                            Entry entry = null;

                            do {
                                try {
                                    entry = (Entry) objectInputStream.readObject();
                                    entries.put(entry.getRelativeFileName(), entry);
                                } catch (final EOFException eof) {
                                    entry = null;
                                }

                            } while (null != entry);
                        }
                    }
                }

                // get current state
                Files.find(Paths.get(rootFolder), Integer.MAX_VALUE, (filePath, fileAttr) -> fileAttr.isRegularFile()).forEach(p -> {

                    final Entry currentEntry = new Entry(p);
                    final Entry previousEntry = entries.get(currentEntry.getRelativeFileName());

                    if (null == previousEntry) {
                        currentEntry.setFound(true);
                        entries.put(currentEntry.getRelativeFileName(), currentEntry);
                        System.out.println("U:" + currentEntry.getRelativeFileName());
                    } else {

                        previousEntry.setFound(true);

                        if (currentEntry.getFileSize() != previousEntry.getFileSize() || currentEntry.getLastModified() != previousEntry.getLastModified()) {
                            previousEntry.setFileSize(currentEntry.getFileSize());
                            previousEntry.setLastModified(currentEntry.getLastModified());
                            System.out.println("U:" + currentEntry.getRelativeFileName());
                        }
                    }
                });

                TimeUnit.SECONDS.sleep(1);

                // update state
                try (final FileOutputStream fileOutputStream = new FileOutputStream(serializeFile)) {
                    try (final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {

                        entries.values().forEach(entry -> {

                            if (entry.wasFound()) {

                                try {
                                    objectOutputStream.writeObject(entry);
                                } catch (final IOException ioe) {
                                    throw new RuntimeException(ioe);
                                }
                            } else {
                                System.out.println("D:" + entry.getRelativeFileName());
                            }
                        });
                    }
                }
            }

        } catch (final Exception e) {
            e.printStackTrace();
            usage();
        }
    }

    private void usage() {
        System.err.println("\nUsage: java -jar dirmonitor-1.0.0-SNAPSHOT.jar serializeFile rootFolder\n");
        System.err.println("    serializeFile: file that current state is compared with and written to");
        System.err.println("    rootFolder   : folder to compare\n");
        System.exit(1);
    }

    private void parseArgs(final String[] args) {

        for (int i = 0; i < ARG_COUNT; i++) {

            if (0 == i) {
                serializeFile = args[i];
            } else {
                rootFolder = args[i];
            }
        }
    }
}
