package org.example.directory;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Path;

public class Entry implements Serializable {

    private static final long serialVersionUID = 3889094554352059283L;

    private long fileSize;
    private long lastModified;
    private final String relativeFileName;
    private transient boolean found = false;

    public Entry(final Path path) {
        final File file = path.toFile();
        fileSize = file.length();
        lastModified = file.lastModified();
        relativeFileName = path.toString();
    }

    public long getFileSize() {
        return fileSize;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getRelativeFileName() {
        return relativeFileName;
    }

    public void setFileSize(final long aFileSize) {
        fileSize = aFileSize;
    }

    public void setLastModified(final long aLastModified) {
        lastModified = aLastModified;
    }

    public boolean wasFound() {
        return found;
    }

    public void setFound(final boolean wasFound) {
        found = wasFound;
    }

    @Override
    public String toString() {
        return "Entry [fileSize=" + fileSize + ", lastModified=" + lastModified + ", relativeFileName=" + relativeFileName + ", found=" + found + "]";
    }
}
